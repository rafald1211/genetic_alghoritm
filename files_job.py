from random import randint


def mutate(chrm):
    i = randint(0, 4)
    i_val = chrm[i]
    if i_val=='0':
        i_val='1'
    else:
        i_val='0'
    return chrm[:i] + i_val + chrm[i + 1:]


print(mutate('11111'))


