# -*- coding: utf-8 -*-

import random
from random import randint
import matplotlib.pyplot as plt
import numpy as np

# http://www.k0pper.republika.pl/geny.htm

# Program umożliwiający znalezienie maksimum funkcji dopasowania jednej zmiennej dla liczb całkowitych w zadanym zakresie
# przy pomocy elementarnego algo genetycznego
#   reprodukcja z uzyciem nieproporcjonalnej ruletki, +
#   krzyzowanie proste,                 +
#   mutacja równomierna)            +

# Program powinien umożliwiać :
#   różne funkcje dopasowania       +
#   populacje o różnej liczebności  +
#   różne parametry operacji genetycznych (krzyżowania i mutacji)   +

# Program powinien zapewnić:
#   wizualizacja wyników w postaci wykresów średniego, minimalnego i maksymalnego przystosowania dla kolejnych populacji oraz wykresu funkci w zadanym przedziale


class Program:

    PARENT_DIE_AFTER_CROSS = True
    PARENT_CHILD_TRY_MAX = 10
    INDEX_CROSS_DIVIDE = 2
    CROSS_PROBABILITY = 0.9
    MUTATION_PROBABILITY = 0.05 #two from ten will be mutated

    DEFAULT_CROSS_CHILD_MIN_AMOUNT = 5
    DEFAULT_CROSS_CHILD_MAX_AMOUNT = 9
    DEFAULT_CROSS_INDEX_MIN = 2
    DEFAULT_CROSS_INDEX_MAX = 4

    ROUNDS_LIMIT = 100

    rounds = {}
    chromosomes = []  # osobniki
    chromosomes_best = dict()
    chromosomes_worst = dict()
    chromosomes_best_worst_avg = dict()

    def __init__(self, factors, domain, input_chromosomes, max_rounds):
        self.factors = factors
        self.domain = self.convert_domain_to_positive(domain)
        self.input_chromosomes = input_chromosomes
        self.max_rounds = max_rounds
        self.bit_length = self.bit_representation_size(domain[-1])
        self.print_start_settings()
        self.start()

    def start(self):
        # ROUND 0
        round_nr = 0
        self.get_random_population(self.input_chromosomes)#start population
        self.rounds[round_nr] = self.chromosomes #
        self.validate_chromosomes()
        self.print_round_info_end(round_nr) #sprawdz z czym zaczyna

        # START LIFE
        while len(self.chromosomes) < self.domain[-1] and len(self.chromosomes)>0 and round_nr<self.max_rounds: #jeśli liczba chromosomów(różnych) jest większa od najwiekszej wartości dziedziny mnoz populacje
            round_nr+=1
            self.print_round_info_begging(round_nr)
            self.selection()
            self.cross_population()
            self.mutate_population()
            self.remove_repeations()
            self.validate_chromosomes()
            self.add_best_chrm()
            self.add_worst_chrm()
            if len(self.chromosomes):
                self.add_best_worst_avg(round_nr)
            self.rounds[round_nr] = self.chromosomes
            self.print_round_info_end(round_nr)

        self.validate_chromosomes()

        self.print_history()
        self.check_repeations()
        self.print_finish_reason(round_nr)
        self.print_best_chrm_rank()
        self.print_worst_chrm_rank()

        self.print_plot()

        del self.chromosomes
        for r in range(round_nr):
            del self.rounds[r]

    def add_best_worst_avg(self, round_nr):
        best = self.get_int(self.get_best_chrm())
        worst = self.get_int(self.get_worst_chrm())
        avg = self.get_avg_chrm(best,worst)
        if best==-1 or worst==-1 or avg ==-1:
            self.chromosomes_best_worst_avg[round_nr] = ['01010', '10110', '01111']
        self.chromosomes_best_worst_avg[round_nr] = [best, worst, avg]

    def convert_domain_to_positive(self, domain):
        positive_domain = []
        for i in range(len(domain)):
            positive_domain.append(i)
        return positive_domain

# RESULTS AND BEST CHRM MATHS

    @staticmethod
    def f_x(x, _factors):
        function_grade = len(_factors) - 1  # jesli 3 wspolczynniki to 2 poziom
        result = 0
        for i in range(0, function_grade + 1):  # from 0 to function_grade + 1
            result += _factors[i] * x ** (function_grade - i)
        return result

    def get_best_chrm(self):
        if len(self.chromosomes)==0:
            return -1
        chrms = self.chromosomes
        first = self.get_int(chrms[0])
        factors = self.factors
        best_chrm_val = self.f_x(first, factors)
        best_chrm = self.chromosomes[0]
        for chrm in chrms:
            curr_val = self.f_x(self.get_int(chrm), factors)
            if curr_val > best_chrm_val:
                best_chrm_val = curr_val
                best_chrm = chrm
        return best_chrm

    def get_avg_chrm(self, best, worst):
        to_return = (best+worst)/2
        return int(to_return)

    def get_worst_chrm(self):
        if len(self.chromosomes) == 0:
            return -1
        chrms = self.chromosomes
        first = self.get_int(chrms[0])
        factors = self.factors
        worst_chrm_value = self.f_x(first, factors)
        worst_chrm = self.chromosomes[0]
        for chrm in chrms:
            curr_val = self.f_x(self.get_int(chrm), factors)
            if curr_val < worst_chrm_value:
                worst_chrm_value = curr_val
                worst_chrm = chrm
        return worst_chrm

    def add_best_chrm(self):
        # if len(self.chromosomes) != 0:
        chromosomes_best = self.chromosomes_best
        best = -1
        if len(chromosomes_best):
            best = self.get_best_chrm()
        if best in chromosomes_best:
            chromosomes_best[best] +=1
        else:
            chromosomes_best[best] = 0
            chromosomes_best[best] += 1

    def add_worst_chrm(self):
        chromosomes_worst = self.chromosomes_worst
        worst = -1
        if len(chromosomes_worst):
            worst = self.get_worst_chrm()
        if worst in chromosomes_worst:
            chromosomes_worst[worst] += 1
        else:
            chromosomes_worst[worst] = 0
            chromosomes_worst[worst] += 1

# RANDOM START POPULATION
    def get_random_population(self, input_amount):
        # for i in range(input_amount):
        left_amount = input_amount
        count = 0
        while left_amount!=0 or count==100:
            count+=1
            chrm = self.generate_chromosome_random(self.bit_length)
            if not self.if_chromosome_exist(chrm,self.chromosomes):
                self.chromosomes.append(chrm)
                left_amount-=1
            else:
                left_amount+=1

    def generate_chromosome_random(self, length):
        random_chromosome = ''
        for i in range(length):
            random_chromosome += str(int(bool(random.getrandbits(1))))
        return random_chromosome

# VALIDATION
    def validate_chromosomes(self):  # ocen osobniki/chromosomy
        chrms = self.chromosomes
        for chrm in chrms:
            if not self.validate_chromosome(chrm):
                self.chromosomes.remove(chrm)

    def validate_chromosome(self, chrm):  # ocen osobniki/chromosomy
        chrm_int = int(chrm,2)
        last_domain = self.domain[-1]
        if chrm_int<=last_domain:
            return True
        else:
            return False

    def remove_repeations(self):
        dictex = self.get_repeations_dict()
        for key in dictex:
            if dictex[key]>1:
                self.chromosomes.remove(key)

    def is_chrm_in_chromosomes(self, chrm, chromosomes):
        if chrm in chromosomes:
            return True
        return False

    def check_repated_chromosomes(self, chromosomes):
        pass

# SELECTION
    def selection(self):#tyle razy losujemy ile jest osobnikow w populacji
        val_sum = self.get_value_sum(self.chromosomes)
        val_dict = self.assign_val(self.chromosomes)
        self.chromosomes = self.turn_wheel(val_dict, val_sum)

    def turn_wheel(self, val_dict, val_sum):
        wheel_list = []
        wheel_result = {}

        # DODAJ WSZYSTKIE CHROMOSOMY DO LISTY
        for val in val_dict:#lista chromosomow
            for i in range(val_dict[val]):#tyle ile chromosom ma wartość (np 5/20) tyle razy (5) zapisujemy do listy
                wheel_list.append(val)

        # PRZYGOTUJ DICTA
        for chrm in val_dict:
            wheel_result[chrm] = 0

        # ZAKREC N RAZY KOLEM I WYLOSUJ INDEXY
        wheel_size = len(wheel_list)
        for i in range(wheel_size):# zakrec tyle razy ile miejsc
            random_index = randint(0,wheel_size-1)
            # bit_index = self.get_bin(random_index)
            wheel_result[wheel_list[random_index]] +=1
            # wheel_result[bit_index] += 1

        # OKRESL CZY PRZETRWAL CZY NIE
        chromosomes_result = {}
        for index in wheel_result:
            if wheel_result[index] > 0:
                chromosomes_result[index] = True
            else:
                chromosomes_result[index] = False

        survived_chromosomes = []
        for chrm in chromosomes_result:
            if chromosomes_result[chrm] == True:
                survived_chromosomes.append(chrm)

        return survived_chromosomes

    def get_value_sum(self,chromosomes):
        return sum(self.get_int(chrm_val) for chrm_val in chromosomes)

    def assign_val(self, chromosomes):
        dict = {}
        for chrm in chromosomes:
            dict[chrm] = self.get_int(chrm)
        return dict


# CROSSING
    def cross_population(self):
        chromosomes_copy = self.chromosomes
        while len(chromosomes_copy)!=0:
            try:#weźmy dwóch rodziców, musi być dwóch !
                if self.PARENT_DIE_AFTER_CROSS:    #jesli umieraja po dzieciach
                    parent1 = chromosomes_copy.pop()
                    parent2 = chromosomes_copy.pop()
                else:#jeśli nie umieraja po dzieciach
                    pass
                    # parent1 = chromosomes_copy.pop()
                    # parent2 = chromosomes_copy.pop()
            except IndexError: #jeśli na końcu został jeden z nich - nic z tego nie będzie
                continue
            else: #jeśli udało się przypisać dwoch rodzicow
                CHILD_AMOUNT = self.get_child_amount() #sprawdz ile dzieci będą mieli
                if CHILD_AMOUNT == 0: #jeśli 0 - lecmy dalej bo dzieci nie będzie
                    continue
                else: #jesli nie 0
                    if self.if_cross():
                        childs = []
                        _child_amount_try = CHILD_AMOUNT
                        while len(childs)!=CHILD_AMOUNT:
                            if _child_amount_try==self.PARENT_CHILD_TRY_MAX: #po 20 razach gdy sie starali rodzice nie poszlo - nie wyjdzie
                                break
                            child = self.cross(parent1, parent2, self.get_random_index_cross())
                            if not self.if_chromosome_exist(child,self.chromosomes) and not self.if_chromosome_exist(child,childs) and self.validate_chromosome(child):
                                childs.append(child)
                                _child_amount_try-=1
                            else:
                                _child_amount_try+=1
                        self.chromosomes = self.chromosomes+childs
                    else:
                        continue
    def cross(self, chrm1, chrm2, ICD = INDEX_CROSS_DIVIDE):
        tmp = chrm1[ICD:] #przechowaj od indeksu podzialu
        chrm1 = chrm1[:ICD]+chrm2[ICD:]
        chrm2 = chrm2[:ICD]+tmp
        if random.getrandbits(1):
            return chrm1
        else:
            return chrm2

    def get_child_amount(self, min=DEFAULT_CROSS_CHILD_MIN_AMOUNT, max=DEFAULT_CROSS_CHILD_MAX_AMOUNT):
        return randint(min,max)

    def get_random_index_cross(self,min=DEFAULT_CROSS_INDEX_MIN, max=DEFAULT_CROSS_INDEX_MAX):
        return randint(min,max)

    def if_cross(self):
        random_val = randint(1,100)
        max_range = self.CROSS_PROBABILITY * 100
        if random_val>=1 and random_val<=max_range: #np. MP = 0,2 => max range = 20 => prawdopodobienstwo, ze random_val>=1 i random_val<=20 => 20/100 => 0,2
            return True
        return False

# MUTATION
    def mutate_population(self):
        for index,chrm in enumerate(self.chromosomes):
            if self.if_mutate():
                mutated_chrm = self.mutate(chrm)
                if self.validate_chromosome(mutated_chrm):
                    self.chromosomes[index] = self.mutate(chrm)
            else:
                continue

    def if_mutate(self):
        random_val = randint(1,100)
        max_range = self.MUTATION_PROBABILITY*100
        if random_val>=1 and random_val<=max_range: #np. MP = 0,2 => max range = 20 => prawdopodobienstwo, ze random_val>=1 i random_val<=20 => 20/100 => 0,2
            return True
        return False


    def mutate(self, chrm):
        i = randint(0, 4)
        i_val = chrm[i]
        if i_val == '0':
            i_val = '1'
        else:
            i_val = '0'
        return chrm[:i] + i_val + chrm[i + 1:]

# UTILS    ########################

    def if_chromosome_exist(self,chrm,chrms):
        return chrm in chrms

    def bit_representation_size(self, integer):
        return len(bin(integer)[2:])

    def get_int(self, bin):
        return int(bin,2)

    def get_bin(self, int):
        return bin(int)[2:]

# PRINTS

    def print_chromosomes(self):
        for chrm in self.chromosomes:
            print(chrm)

    def print_history(self):
        for r_nr,val in self.rounds.items():
            print("round: {} -> population_size: {} -> population: {}".format(r_nr, len(val), val))

    def print_round_info_begging(self, round_nr):
        print("start of round nr: {}".format(round_nr))

    def print_round_info_end(self, round_nr):
        print("END OF ROUND NR.: {}, POPULATION SIZE: {}, \nIT IS POPULATION: ".format(round_nr, len(self.rounds[round_nr])))
        for chrm in self.rounds[round_nr]:
            int_repr = self.get_int(chrm)
            print("Chromosome bin: {}, chromosome int: {}, f({}) = {}"
                  .format(chrm, int_repr, int_repr, self.f_x(int_repr, self.factors)))
        print("\n")

    def print_start_settings(self):
        print("Input population: {} \n"
              "".format(self.input_chromosomes))

    def print_best_chrm_in_population(self):
        best_chromosome = self.get_best_chrm()
        if best_chromosome != 'NULL':
            print("BEST CHROMOSOM IN POPULATION: {} <==> f({}) = {}".
                  format(best_chromosome, self.get_int(best_chromosome),self.f_x(self.get_int(best_chromosome),self.factors)))
        else:
            print("ni ma ")

    def print_best_chrm_rank(self):
        print("BEST CHROMOSOMES")
        for chrm,val in self.chromosomes_best.items():
            if chrm != -1:
                print("Chromosome: {} <=> {} with {} occurrences".format(chrm,self.get_int(chrm), val ))

    def print_worst_chrm_rank(self):
        print("WORST CHROMOSOMES")
        for chrm,val in self.chromosomes_worst.items():
            if chrm != -1:
                print("Chromosome: {} <=> {} with {} occurrences".format(chrm,self.get_int(chrm), val ))

    def print_finish_reason(self, round_nr):
        print("end because condition first is {}.If false population is equal of max domain".format(
            len(self.chromosomes) < self.domain[-1]))
        print("end because condition second is {}. If false population has been died and no one survived".format(
            len(self.chromosomes) > 0))
        print("end because condition third is {}. If false population has been died and no one survived".format(
            round_nr < self.max_rounds))

    def print_plot(self):

        plot_coords = [-3, 23, -5, 33]
#WYKRES ZADANIE

        plot_domain = domains_dec
        plot_values = [Program.f_x(x, factors) for x in plot_domain]
        plt.axis(plot_coords)
        plt.plot(domains_dec, plot_values, 'ro')
# MIN MAX AVG
        max = []
        min = []
        avg = []
        for round,values in self.chromosomes_best_worst_avg.items():
            max.append(values[0])
            min.append(values[1])
            avg.append(values[2])
# MIN
        max_values = [Program.f_x(i, self.factors) for i in max]
        min_values = [Program.f_x(i, self.factors) for i in min]
        avg_values = [Program.f_x(i, self.factors) for i in avg]

        plt.scatter(max,max_values, s=200, c='blue',    alpha=0.01 )
        plt.scatter(min,min_values, s=200, c='green',   alpha=0.2)
        plt.scatter(avg,avg_values, s=200, c='yellow',  alpha=0.07 )

        plt.grid(True)
        plt.show()

#WR


    # REPEATIONS

    def has_repeations(self):
        ex = self.chromosomes
        dictex = {}
        for e in ex:
            dictex[e] = 0
        for e in ex:
            dictex[e] += 1
        for key in dictex:
            if dictex[key]>1:
                return True
        return False

    def get_repeations_dict(self):
        ex = self.chromosomes
        dictex = {}
        for e in ex:
            dictex[e] = 0
        for e in ex:
            dictex[e] += 1
        return dictex

    def check_repeations(self):
        ex = self.chromosomes
        dictex = {}
        for e in ex:
            dictex[e] = 0
        for e in ex:
            dictex[e] +=1
        for e in dictex:
            print("chromosome {} has {} clones".format(e, dictex[e]))
        return dictex

factors = [-0.25, 5, 6]
domains_dec = list(range(-1, 21))
domains_bin = [bin(x) for x in domains_dec]

ins = Program(factors,domains_dec, input_chromosomes=12, max_rounds=200)
# print(ins.print_chromosomes)