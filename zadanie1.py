factors = [-0.25, 5, 6]
domains_dec = range(-1, 22)
domains_bin = [bin(x) for x in domains_dec]

def f_x(x, _factors=factors):
	function_grade = len(_factors) - 1  # jesli 3 wspolczynniki to 2 poziom
	result = 0

	for i in range(0, function_grade + 1):  # from 0 to function_grade + 1
		result += _factors[i] * x ** (function_grade - i)

	return result

def get_dict_with_results_for_domain(_factors=factors, _domains=domains_dec):
	result_dict = {}
	for x in domains_dec:
		result_dict[x] = f_x(x)
	return result_dict

def print_dict(_dict):
	for x in domains_dec:
		print("f({}) = {}".format(x, _dict[x]))


def save_results_to_file(_filename, _results):
	pass

def get_bin(int):
	return bin(int)[2:]

def get_bin_len(int):
	return bin(int)[2:]


def get_minimum_delta(domains=domains_dec):
	bit_quantity = len((bin(len(domains))[2:]))
	licznik = domains[-1]-domains[0]
	mianownik = 2**bit_quantity-1
	minimum_x_delta = float(licznik/mianownik)
	return minimum_x_delta

def get_domain_delta_bin(min_delta=1, domains=domains_dec):
	domain_str_bit = []
	max = domains[-1]
	min = domains[0]
	counter = 0
	while max>min:
		domain_str_bit.append(bin(counter))
		counter +=1
		max -=min_delta
	return domain_str_bit



domains_delta_bin = get_domain_delta_bin(get_minimum_delta(domains_dec))
domains_delta_bin = get_domain_delta_bin()

print(domains_delta_bin)

print_dict(get_dict_with_results_for_domain(factors,domains_dec))